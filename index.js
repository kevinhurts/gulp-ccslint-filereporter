'use strict';

var fs = require('fs-extra');
var gutil = require('gulp-util');
var conf = '';

var fileReporter = {
  reporter: constructor,
  setConfig : function( newConf ){
    conf = newConf;
  }
}

function constructor( file ) {

  var wrStream;
  var out = [];
  var results = file.csslint.results;

  var treshhold  = conf.treshhold || 0;
  var dir = conf.directory || './logs/';

  var filename =  dir + file.path.replace(/^.*[\\\/]/, '') + '.log';

  // Check if directory exists. If not: creates it
  fs.ensureDir(dir);

  if (wrStream && filename !== filename) {
      wrStream.end();
      wrStream = null;
  }

  if (!wrStream) {
      wrStream = fs.createWriteStream(filename);
  }

  results.forEach(function (result, i) {
      var err = result.error;

      // If the treshhold is higher as the amount of errors the errors will be shown in the command prompt
      if( results.length > treshhold ){
        if (i === 0) {
            // Show error message in commandline
            gutil.log( gutil.colors.red( results.length + ' lint error(s)') + ' logged in file ' + gutil.colors.magenta( filename ) );
            // Add header to log file
            out.push(results.length + ' lint error(s) in file ' + result.file);
        }
        // Show where the error occured
        out.push('  [' + err.line + ',' + err.col + '] ' + err.message);
      } else {
        if (i === 0) {
            gutil.log( gutil.colors.red( results.length + ' lint error(s)') + ' in file ' + gutil.colors.cyan( result.file ) );
        }
        // Show errors in command prompt
        gutil.log( gutil.colors.cyan( '[' + err.line + ',' + err.col + '] ') + err.message );
      }

  });

  if( results.length > treshhold ){
    wrStream.write(out.join('\n'));
  }
};

module.exports = fileReporter;
