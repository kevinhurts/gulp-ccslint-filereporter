##GULP CSSLINT FILEREPORTER 

A custom filereporter for the gulp-csslint module (https://www.npmjs.org/package/gulp-csslint)

#Implementation

```
var fileReporter = require('gulp-csslint-filereporter');

// set config
fileReporter.setConfig({
    "treshhold": 4, 
    // the treshhold is the number of errors it needs to start writing log files (set to 0 if you always want log files)
    "directory": ""
    // the directory to where the log files are written set empty if you want log files next to the original files (default = './log')
});

// use filereporter as custom reporter
return gulp.src( sources.css )
    .pipe( csslint() )
    .pipe( csslint.reporter( fileReporter.reporter ) );
```
